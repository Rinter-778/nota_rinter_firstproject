local sensorInfo = {
    name = "UpdateFarkList",
    desc = "Updates fark list with the new bought units ",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Updates fark list with the new bought units
return function(farkList)
    local teamID = Spring.GetMyTeamID()
    local myunits = Spring.GetTeamUnits(teamID)

    for i = 1, #myunits do
        local unitId= myunits[i]
        local defID = Spring.GetUnitDefID(unitId)
        if defID ~= nil and Spring.ValidUnitID(unitId) and UnitDefs[defID].name == 'armfark' then             

                if farkList[unitId] == nil then 
                    farkList[unitId] = {}
                    farkList[unitId].taken = false
                    farkList[unitId].id = unitId
                end
        end
    end

    for id, v in pairs(farkList) do
        if not Spring.ValidUnitID(id) then
            farkList[id] = nil
        end
    end
    return farkList
end