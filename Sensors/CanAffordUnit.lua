local sensorInfo = {
    name = "CanAffordUnit",
    desc = "Returns true if you have enough metal for unit",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns true if you have enough metal for unit
return function(unitName, level)

    local teamID = Spring.GetMyTeamID()
    
    local metalAmount = Spring.GetTeamResources (teamID, "metal" )

    if unitName == "lineUpgrade" then
        return 300 * level <= metalAmount 
    end 

    for id,unitDef in pairs(UnitDefs) do
        if unitName == unitDef.name then 
            return unitDef.metalCost <= metalAmount
        end
    end

    return false
end