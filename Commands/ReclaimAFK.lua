function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Free unit for use",
		parameterDefs = {
			{ 
				name = "busyUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "busyUnits",
			},
            { 
				name = "lines",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "lines",
			},
		}
	}
end


local function ClearState(self)
end

function Run(self, units, parameter)

    local units = parameter.busyUnits.units
    local lines = parameter.lines
    local teamID = Spring.GetMyTeamID()
	for id, busy in pairs(units) do
        if not Spring.ValidUnitID(id) then 
            units[id] = nil
        else    
            local defID = Spring.GetUnitDefID(id)

            if defID ~= nil then 
                if UnitDefs[defID].name == 'armbox'  then 
                    local radius = 1000
                    if UnitDefs[defID].name == 'armmart' then 
                        radius = 1350
                    end
                    local x,y,z = Spring.GetUnitPosition(id)
                    local pos = Vec3(x,y,z)
                    if pos:Distance(lines.Mid.pos) > 500 and pos:Distance(lines.Top.pos) > 500 and pos:Distance(lines.Bot.pos) > 500 then

                        local nearUnits = Spring.GetUnitsInSphere(x,y,z, radius)
                        local safe = true
                        for k = 1, #nearUnits do
                            local enId = nearUnits[k]
                            if Spring.ValidUnitID(enId) then 
                                local unitTeamId = Spring.GetUnitTeam(enId)
                                if not Spring.AreTeamsAllied(unitTeamId, teamID)  then 
                                    safe = false
                                    break
                                end
                            end
                        end
                        if safe then 
                            units[id] = false
                        end
                    end
                end
            end
        end
    end
	return SUCCESS
end


function Reset(self)
	ClearState(self)
end
