local sensorInfo = {
	name = "CommanderPos",
	desc = "Returns position of the commander",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Returns position of the commander
return function()
	local commanderPos = Vec3(0,0,0);
	local furthestDist = 0
	for i=1, #units do		
		local unitID = units[i]
		if UnitDefs[Spring.GetUnitDefID(unitID)].name == "armbcom" then
			local x,y,z = Spring.GetUnitPosition(unitID)
			commanderPos = Vec3(x,y,z)
			break
		end
	end	
	return commanderPos
end