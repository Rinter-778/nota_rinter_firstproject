local sensorInfo = {
	name = "GetDangerMap",
	desc = "Returns units array",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

local SprHeight = Spring.GetGroundHeight
local Echo = Spring.Echo
local mapX =  Game.mapSizeX --/ Game.squareSize -- Game.mapSizeX Divide by Game.squareSize to get engine's "mapDims" coordinates
local mapZ =  Game.mapSizeZ --/ Game.squareSize -- Game.mapY
local resolution = 100
local maxHeightOffset = 650

function WriteToFile(map)
	local filename = "dangerMap.ppm"
	local file = io.open(filename, "w")
	
	if file==nil then
		Echo("Couldn't open file")
	else
		file:write("P2\n" .. resolution .. " " .. resolution .. "\n" .. "50" .. "\n")
		for z = 1, resolution do
			for x = 1, resolution do
				file:write(tostring(map[x][z].danger) .. " ")
			end
			file:write('\n')
		end	
		file:close()
	end	
end


-- @Returns units array
return function(enemyPositionTable, dangerMap)	
	if not enemyPositionTable.dirty then
		return dangerMap
	end
	dangerMap.dirty = true
	enemyPositionTable.dirty = false

	local scaleX = dangerMap.scaleX
	local scaleZ = dangerMap.scaleZ
	
	for enemyDefID, enemyTable in pairs(enemyPositionTable.enemies) do
		
		local enemyDef = UnitDefs[enemyDefID]
		local weaponDef = WeaponDefs[enemyDef.weapons[1]]
		
		local weaponRange = enemyDef.maxWeaponRange + 100		
		if weaponRange == nil then
			weaponRange = 0
		end

		if enemyDef.canMove then
			weaponRange = weaponRange + 100
		end

		--Echo(weaponRange)
		
		local projSpeed = 0
		if weaponDef ~= nil then
			projSpeed = weaponDef.projectilespeed
		end
		
		for enemyID, enemy in pairs(enemyTable) do
			if enemy.dirty then 
				enemy.dirty = false
				local enemyPosition = enemy.pos
				local startX = math.max(1, math.floor((enemyPosition.x - weaponRange)/scaleX))
				local stopX = math.min(resolution, math.ceil((enemyPosition.x + weaponRange)/scaleX))
				
				local startZ = math.max(1, math.floor((enemyPosition.z - weaponRange)/scaleZ))
				local stopZ = math.min(resolution, math.ceil((enemyPosition.z + weaponRange)/scaleZ))
				
				for x = startX, stopX do
					for z = startZ, stopZ do
						local mapPos = Vec3(x * scaleX - scaleX/2,dangerMap[x][z].height ,z * scaleZ - scaleZ/2)
						local distance = mapPos:Distance(enemyPosition)
						if  distance < weaponRange and mapPos.y + maxHeightOffset > enemyPosition.y then  --and Spring.GetUnitWeaponHaveFreeLineOfFire( enemyID, 0, mapPos.x, mapPos.y, mapPos.z )
							dangerMap[x][z].danger = dangerMap[x][z].danger +  1 -- (weaponRange * (projSpeed/3) - distance)/10 
						end
					end
				end
			end
		end
	end
	
	--WriteToFile(dangerMap)
	
	return dangerMap
end


