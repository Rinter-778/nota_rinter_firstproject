local sensorInfo = {
    name = "canAffordUnit",
    desc = "Returns true if enemy units are in range",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns true if you have enough metal for unit
return function(id, radius)

    local teamID = Spring.GetMyTeamID()
    
    local x,y,z = Spring.GetUnitPosition(id)
    if x == nil then
        return true
    end
    local units = Spring.GetUnitsInSphere(x,y,z, radius)

    for k = 1, #units do
        local unitTeamId = Spring.GetUnitTeam(units[k])
        local enId = units[k]
        if not Spring.AreTeamsAllied(unitTeamId, teamID)  then 
            local defId = Spring.GetUnitDefID(enId)
			if defId ~= nil and UnitDefs[defId].isBuilding then 
                return true
            end
        end
    end

    return  false
end