-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Get unit in param",
        parameterDefs = {
            { 
                name = "unitName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "'armbox'",
            },
            { 
                name = "busyUnits",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "busyUnits",
            },
            { 
                name = "outputID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

function Run(self, units, parameter)

    local busyUnits = parameter.busyUnits.units
    local name = parameter.unitName
    local outputID = parameter.outputID

    if self.timer == nil and busyUnits ~= nil then
        for id, busy in pairs(busyUnits) do
            local defID = Spring.GetUnitDefID(id)
            if not Spring.ValidUnitID(id) then 
                busyUnits[id] = nil
            elseif defID ~= nil and UnitDefs[defID].name == name and not busy then 
                outputID.id = id 
                busyUnits[id] = true
                return SUCCESS
            end
        end
    end

    if self.timer == nil and not parameter.busyUnits.shopped and Sensors.nota_rinter_firstproject.CanAffordUnit(name)  then 
        parameter.busyUnits.shopped = true
        message.SendRules({
            subject = "swampdota_buyUnit",
            data = {
                unitName = name
            },
        })
        self.timer = 2
    end

    if self.timer ~= nil then
        self.timer = self.timer - 1
        if self.timer == 0 then 
            local teamID = Spring.GetMyTeamID()
            local myunits = Spring.GetTeamUnits(teamID)
            
            for i = 1, #myunits do
                local unitId = myunits[i]
                local defID = Spring.GetUnitDefID(unitId)

                if defID ~= nil and Spring.ValidUnitID(unitId) and UnitDefs[defID].name == name and
                   (busyUnits[unitId] == nil or not busyUnits[unitId]) then                     
                    busyUnits[unitId] = true
                    outputID.id = unitId
                    return SUCCESS
                end
            end
        end
    end

    return RUNNING
end


function Reset(self)
    self.timer = nil
    return self
end