function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load all units into transporter",
		parameterDefs = {
			{ 
				name = "loaderID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end



-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.hasOrder = nil
end

function Run(self, units, parameter)
	local loaderID = parameter.loaderID -- id
	local loaderDef = UnitDefs[Spring.GetUnitDefID(loaderID)]
	
	local unitsInside = Spring.GetUnitIsTransporting(loaderID)
	
	if unitsInside == nil or #unitsInside == 0 then
		return SUCCESS
	end
	
	local commandsPresent = Spring.GetCommandQueue(loaderID,-1)
	if (commandsPresent ~= nil) and (commandsPresent.id == CMD.UNLOAD_UNITS) then
		return RUNNING
	end
	
	-- validation
	if loaderDef.transportCapacity == 0 then
		return FAILURE
	end

	--for i = 1, #unitsInside do
		--local unitID = unitsInside[i]
		--SpringGiveOrderToUnit(loaderID, CMD.UNLOAD_UNIT, SpringGetUnitPosition(loaderID), {})		
	--end
	if self.hasOrder == nil or self.hasOrder == 0 then 
		self.hasOrder = 30
		local x,y,z = SpringGetUnitPosition(loaderID)
		SpringGiveOrderToUnit(loaderID, CMD.UNLOAD_UNITS, {x, y, z,200}, {})		
	end
	self.hasOrder = self.hasOrder - 1
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
