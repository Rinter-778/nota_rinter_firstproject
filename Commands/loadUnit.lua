function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load all units into transporter",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "loaderID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.hasOrder = nil
end

function Run(self, units, parameter)
	local loaderID = parameter.loaderID -- id
	local loaderDef = UnitDefs[Spring.GetUnitDefID(loaderID)]
	
	local unitID = parameter.unitID
	local unitDef = UnitDefs[Spring.GetUnitDefID(unitID)]

	-- validation
	if not Spring.ValidUnitID(loaderID) or not Spring.ValidUnitID(unitID) or loaderDef == nil or unitDef == nil or loaderDef.transportCapacity == 0 or unitDef.cantBeTransported then
		return FAILURE
	end
	
	local isSuccess = true


	local unitTransID = Spring.GetUnitTransporter(unitID)
	
	if unitTransID ~= loaderID then
		isSuccess = false
		if self.hasOrder == nil or self.hasOrder == 0 then			
			self.hasOrder = 20
			SpringGiveOrderToUnit(loaderID, CMD.LOAD_UNITS, {unitID}, {})
		end
		self.hasOrder = self.hasOrder - 1
	end
	
	if isSuccess then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
