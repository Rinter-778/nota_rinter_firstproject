local sensorInfo = {
    name = "GetEnemies",
    desc = "Returns units that need to be rescued",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns units that need to be rescued
return function()
    local teamID = Spring.GetMyTeamID()
    local myunits = Spring.GetTeamUnits(teamID)
    local result = {}
    local rescueCircle = Sensors.core.MissionInfo().safeArea
    for i = 1, #myunits do
        local id = myunits[i]
        local defID = Spring.GetUnitDefID(id)
        local x,y,z = Spring.GetUnitPosition(id)
        local pos  = Vec3(x,y,z)
        if not UnitDefs[defID].cantBeTransported and pos:Distance(rescueCircle.center) > rescueCircle.radius then
            result[#result + 1] = id
        end


    end

    return result
end