function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move units to specified positions",
		parameterDefs = {
			{ 
				name = "customUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "positions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 0
local THRESHOLD_STEP = 25

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.thresholds = nil
	self.lastPositions = nil
end

function Run(self, units, parameter)		
	local positions = parameter.positions -- array of Vec3
	
	local locUnits = parameter.customUnits

	local isSuccess = true

	-- first init
	if  self.lastPositions == nil then
		isSuccess = false
		self.lastPositions = {}
		for i = 1, #locUnits do
			local unitID = locUnits[i]
			local x, y, z = SpringGetUnitPosition(unitID)
			SpringGiveOrderToUnit(unitID, CMD.MOVE, positions[i]:AsSpringVector(),{})
			self.lastPositions[unitID] = Vec3(x,0,z) 
		end
	else	
		for i = 1, #locUnits do
			local unitID = locUnits[i]
			local x, y, z = SpringGetUnitPosition(unitID)
			local pos = Vec3(x,0,z)
			SpringGiveOrderToUnit(unitID, CMD.MOVE, positions[i]:AsSpringVector(),{})

			if  self.lastPositions[unitID]:Distance(pos) > 0.1 then
				isSuccess = false
			end
					
			self.lastPositions[unitID] = pos
		end
	end
	
	if isSuccess then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
