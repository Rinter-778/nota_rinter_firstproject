function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Collect metal",
		parameterDefs = {

			{ 
				name = "farkID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end



-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.repeatOrder = nil
end

function Run(self, units, parameter)

	if not Spring.ValidUnitID(parameter.farkID) then 
		return FAILURE
	end

	if self.repeatOrder == nil then 
		self.repeatOrder = 10	
		local x,y,z = SpringGetUnitPosition(parameter.farkID)
		SpringGiveOrderToUnit(parameter.farkID, CMD.RECLAIM, {x,y,z, 1500 }, {})
		self.lastPos = Vec3(x,y,z)	
	end  

	if self.repeatOrder == 0 then 

		local x,y,z = SpringGetUnitPosition(parameter.farkID)
		local pos = Vec3(x,y,z)	

		if pos:Distance(self.lastPos) < 5 then 
			return SUCCESS
		end		
		self.lastPos = pos 
		self.repeatOrder = 10
		SpringGiveOrderToUnit(parameter.farkID, CMD.RECLAIM, {x,y,z, 1500 }, {})
	end

	self.repeatOrder = self.repeatOrder - 1 


	return RUNNING
end


function Reset(self)
	ClearState(self)
end
