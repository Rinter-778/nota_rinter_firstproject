function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Free unit for use",
		parameterDefs = {
			{ 
				name = "id",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "busyUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "multipleUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},

		}
	}
end


local function ClearState(self)
end

function Run(self, units, parameter)

	local units = parameter.busyUnits.units
	if multipleUnits then 
		local ids = parameter.id
		for i = 1,#ids do
			local id = ids[i]
			if units[id] ~= nil then
				if not Spring.ValidUnitID(id) then
					units[id] = nil
				else
					units[id] = false
				end
			end
		end
		return SUCCESS
	end

	if units[parameter.id] ~= nil then
		if not Spring.ValidUnitID(parameter.id) then
			units[parameter.id] = nil
		else
			units[parameter.id] = false
		end
	end
	return SUCCESS
end


function Reset(self)
	ClearState(self)
end
