function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Rescue units with rescuers",
		parameterDefs = {

			{ 
				name = "rescuers",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitsToSave",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "dangerMap",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local State = {
	findRescuePosition = 1, 
	readyForRescue = 2,  
	finished = 3, 
	dead = 4, 
	followPathToRescue = 5,
	pickupUnit = 6,
	returnWithSaved = 7,
	dropUnit = 8,

}

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.rescuers = nil
end

local function GoToRescuePosition(rescuer, rescueCircle)
	local resID = rescuer.id
	local x,y,z = SpringGetUnitPosition(resID)

	if not rescuer.hasMoveOrder then 
		SpringGiveOrderToUnit(resID, CMD.MOVE, rescueCircle.center:AsSpringVector() ,{})
		rescuer.hasMoveOrder = true
	end

	if Vec3(x,rescueCircle.center.y,z):Distance(rescueCircle.center) < rescueCircle.radius - 50 then
		rescuer.hasMoveOrder = false
		return State.readyForRescue
	end
	return State.findRescuePosition
end

local function FollowPath(rescuer)
	local resID = rescuer.id
	local x,y,z = SpringGetUnitPosition(resID)
	local pos = Vec3(x,0,z)
	if rescuer.path[rescuer.pathIndex]:Distance(pos) < 40 then 
		rescuer.pathIndex = rescuer.pathIndex + 1
	end

	if rescuer.pathIndex == #rescuer.path + 1 then
		rescuer.pathIndex = #rescuer.path
		return State.pickupUnit
	end

	SpringGiveOrderToUnit(resID, CMD.MOVE, rescuer.path[rescuer.pathIndex]:AsSpringVector(),{})

	return State.followPathToRescue
end

local function ReturnWithSaved(rescuer)
	local resID = rescuer.id
	local x,y,z = SpringGetUnitPosition(resID)
	local pos = Vec3(x,0,z)
	if rescuer.path[rescuer.pathIndex]:Distance(pos) < 60 then 
		rescuer.pathIndex = rescuer.pathIndex - 1
	end

	if rescuer.pathIndex == 0 then
		return State.dropUnit
	end

	SpringGiveOrderToUnit(resID, CMD.MOVE, rescuer.path[rescuer.pathIndex]:AsSpringVector(),{})

	return State.returnWithSaved
end

local function PickUp(rescuer, saveID)
	if not rescuer.hasPickUpOrder then 
		rescuer.hasPickUpOrder = true
		SpringGiveOrderToUnit(rescuer.id, CMD.LOAD_UNITS, {saveID}, {})
	end

	if Spring.GetUnitTransporter(saveID) == rescuer.id then
		rescuer.hasPickUpOrder = false
		return State.returnWithSaved
	end
	return State.pickupUnit
end

local function DropUnit(rescuer, saveID ,rescueCircle)
	if not rescuer.hasDropUpOrder then 
		rescuer.hasDropUpOrder = true
		local point =  Vec3(rescueCircle.center.x,rescueCircle.center.y,rescueCircle.center.z)
		--point.x = point.x + math.random(-rescueCircle.radius/2, rescueCircle.radius/2 )
		--point.z = point.z + math.random(-rescueCircle.radius/2, rescueCircle.radius/2 )
		SpringGiveOrderToUnit(rescuer.id, CMD.UNLOAD_UNITS,{point.x, point.y, point.z, rescueCircle.radius}, {})
	end

	if Spring.GetUnitTransporter(saveID) ~= rescuer.id then
		local x,y,z = Spring.GetUnitPosition(saveID)
		SpringGiveOrderToUnit(saveID, CMD.MOVE, {x,0,z + 1}, {})

		rescuer.hasDropUpOrder = false
		return State.readyForRescue
	end
	return State.dropUnit
end

function Run(self, units, parameter)

	if self.rescuers == nil then
		self.rescueCircle = Sensors.core.MissionInfo().safeArea
	
		self.rescuers = {}
		for i = 1, #parameter.rescuers do
			self.rescuers[i] = {}
			self.rescuers[i].id = parameter.rescuers[i]
			self.rescuers[i].state = State.findRescuePosition
		end

		self.unitsToSave = {}
		for i = 1, #parameter.unitsToSave do
			self.unitsToSave[i] = {}
			self.unitsToSave[i].id = parameter.unitsToSave[i]
			self.unitsToSave[i].isBeingSaved = false
		end
		self.unitsToSave.counter = 1
	end

	local oneReadyForRescue = true
	for i = 1, #self.rescuers do
		local rescuer = self.rescuers[i]
		if Spring.ValidUnitID(rescuer.id) then
			if rescuer.state == State.findRescuePosition then
				rescuer.state = GoToRescuePosition(rescuer, self.rescueCircle)
			elseif rescuer.state == State.readyForRescue and oneReadyForRescue then
				oneReadyForRescue = false
				
				local unit = nil
				for k = 1,#self.unitsToSave do
					local tmpunit = self.unitsToSave[k]
					if not tmpunit.isBeingSaved and Spring.ValidUnitID(tmpunit.id) then
						rescuer.saveUnitIndex = k
						unit = tmpunit
						break
					end
				end
				if unit ~= nil then 
					unit.isBeingSaved = true
					rescuer.path = Sensors.nota_rinter_firstproject.GetSavePath(rescuer.id, unit.id, parameter.dangerMap)
					rescuer.pathIndex = 1
					rescuer.state = State.followPathToRescue
				else
					rescuer.state = State.finished
				end								
			elseif rescuer.state == State.followPathToRescue then
				rescuer.state = FollowPath(rescuer)
			elseif rescuer.state == State.pickupUnit then
				rescuer.state = PickUp(rescuer, self.unitsToSave[rescuer.saveUnitIndex].id)
			elseif rescuer.state == State.returnWithSaved then
				
				rescuer.state = ReturnWithSaved(rescuer)
			elseif rescuer.state == State.dropUnit then
				rescuer.state = DropUnit(rescuer,self.unitsToSave[rescuer.saveUnitIndex].id ,self.rescueCircle)
			end
		elseif rescuer.state ~= State.dead then
			rescuer.state = State.dead
			self.unitsToSave[rescuer.saveUnitIndex].isBeingSaved = false			
		end
	end

	local allDead = true
	for i = 1, #self.rescuers do
		if self.rescuers[i].state ~= State.dead then
			allDead = false
			break
		end
	end

	if allDead then
		return FAILURE
	end

	for i = 1, #self.rescuers do
		if self.rescuers[i].state ~= State.finished and self.rescuers[i].state ~= State.dead then
			return RUNNING
		end
	end

	return SUCCESS
end


function Reset(self)
	ClearState(self)
end
