local sensorInfo = {
	name = "GetSavePath",
	desc = "Returns array of points on path",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function reconstructPath(current, searchMap, scaleX, scaleZ)
	local result = {}
	while searchMap[current.x][current.z].prev ~= nil do
		result[#result + 1] = current
		current = searchMap[current.x][current.z].prev
	end
	if #result < 4 then 
		return {Vec3(result[#result].x * scaleX - scaleX/2, 0, result[#result].z * scaleZ - scaleZ/2)}
	end

	local finalResut = {}
	local direction = (result[4] - result[1]):GetNormalized()
	finalResut[1] = Vec3(result[1].x * scaleX - scaleX/2, 0, result[1].z * scaleZ - scaleZ/2)
	for i = 2, #result - 4 do 
		local newDirection = (result[i] - result[i+4]):GetNormalized()
		if newDirection:Dot(direction) < 0.90 then 
			direction = newDirection
			finalResut[#finalResut + 1] = Vec3(result[i].x * scaleX - scaleX/2, 0, result[i].z * scaleZ - scaleZ/2)
		end
	end

	finalResut[#finalResut + 1] = Vec3(result[#result - 2].x * scaleX - scaleX/2, 0, result[#result - 2].z * scaleZ - scaleZ/2)
	finalResut[#finalResut + 1] = Vec3(result[#result].x * scaleX - scaleX/2, 0, result[#result].z * scaleZ - scaleZ/2)
	return finalResut
end

local State = {
    Closed=1,
    Open=2,
    NotFound=3,
}

function GetNeighbours(currentPoint,searchMap)
	local directions = {
		Vec3(1,0,0),
		Vec3(0,0,1),
		Vec3(1,0,1),
		Vec3(1,0,-1),
		Vec3(-1,0,1),
		Vec3(-1,0,-1),
		Vec3(0,0,-1),
		Vec3(-1,0,0),
	}
	local result = {}
	for i = 1, #directions do
		local tmp = currentPoint + directions[i]
		if tmp.x > 0 and tmp.x < #searchMap and
		   tmp.z > 0 and tmp.z < #searchMap and 
		   searchMap[tmp.x][tmp.z].state ~= State.Closed then
			result[#result + 1] = tmp
		end
	end
	return result
end

function GetNext(fringe, searchMap)
	if #fringe == 0 then
		return nil
	end

	local tmp 
	local val = 99999999
	local finalIndex = 0

	for i = 1, #fringe do 
		local newTmp = fringe[i]
		local newVal = searchMap[newTmp.x][newTmp.z].fscore
		if newVal < val then
			tmp = newTmp
			val = newVal
			finalIndex = i
		end
	end
	
	for i = finalIndex , #fringe - 1 do
		fringe[i] = fringe[i + 1]
	end
	fringe[#fringe] = nil

	return tmp
end


-- @Returns array of points on path
return function(rescuerID, unitToSaveID, dangerMap, pos)

	if rescuerID == nil then 
		Spring.Echo("Nil rescuer")
	end
	local scaleX = dangerMap.scaleX
	local scaleZ = dangerMap.scaleZ
	local resolution = dangerMap.resolution

	local x,y,z = Spring.GetUnitPosition(rescuerID)
	local resPos = Vec3(x,y,z)
	
	local savePos
	if pos == nil then 
		x,y,z = Spring.GetUnitPosition(unitToSaveID)
		savePos = Vec3(x,y,z)
	else 
		savePos = pos
	end
	
	local resX = math.floor(resPos.x/scaleX + 0.5)
	local resZ = math.floor(resPos.z/scaleZ + 0.5)

	local saveX = math.floor(savePos.x/scaleX + 0.5)
	local saveZ = math.floor(savePos.z/scaleZ + 0.5)
	local dest =  Vec3(resX, 0,resZ)

	local currentPoint = Vec3(saveX, 0, saveZ)
	
	local searchMap = {}
	
	for x = 1, resolution do
		searchMap[#searchMap + 1] = {}
		for z = 1, resolution do
			searchMap[x][z] = {}
			searchMap[x][z].state = State.NotFound
			searchMap[x][z].gscore = 99999999
		end
	end

	local currnetNode = searchMap[currentPoint.x][currentPoint.z]
	currnetNode.state = State.Open
	currnetNode.fscore = currentPoint:Distance(dest)
	currnetNode.gscore = 0
	currnetNode.prev = nil

	local fringe = {currentPoint}

	while #fringe > 0 do 
		currentPoint = GetNext(fringe, searchMap)
		
		if currentPoint == dest then
			return reconstructPath(currentPoint, searchMap, scaleX, scaleZ)
		end
		
		local neighbours = GetNeighbours(currentPoint, searchMap)
		for i = 1, #neighbours do
			local neighbour = neighbours[i]
			local neighbourNode = searchMap[neighbour.x][neighbour.z]
			local heightdiff = math.abs( dangerMap[neighbour.x][neighbour.z].height - dangerMap[currentPoint.x][currentPoint.z].height)
			if dangerMap[currentPoint.x][currentPoint.z].height < 200 and heightdiff < 50 then 
				heightdiff = 0
			end
			
			local tmp = searchMap[currentPoint.x][currentPoint.z].gscore + neighbour:Distance(currentPoint) + dangerMap[neighbour.x][neighbour.z].danger * 500 + (heightdiff * heightdiff) / 250

			if tmp < neighbourNode.gscore then 

				neighbourNode.prev = currentPoint
				neighbourNode.gscore = tmp 
				neighbourNode.fscore = tmp + neighbour:Distance(dest) 

				if neighbourNode.state == State.NotFound then 
					fringe[#fringe + 1] = neighbour
					neighbourNode.state = State.Open
				end

			end
		end
		searchMap[currentPoint.x][currentPoint.z].state = State.Closed
	end
	
	return nil
end