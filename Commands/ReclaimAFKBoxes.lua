function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Free unit for use",
		parameterDefs = {
			{ 
				name = "busyUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "busyUnits",
			},
		}
	}
end


local function ClearState(self)
end

function Run(self, units, parameter)

    local units = parameter.busyUnits.units
    local teamID = Spring.GetMyTeamID()
	for id, busy in pairs(units) do
        if not Spring.ValidUnitID(id) then 
            units[id] = nil
        else    
            local defID = Spring.GetUnitDefID(id)

            if defID ~= nil then 
                if UnitDefs[defID].name == 'armbox' or UnitDefs[defID].name == 'armmart' or UnitDefs[defID].name == 'armmav' then 
                    local x,y,z = Spring.GetUnitPosition(id)
                    local nearUnits = Spring.GetUnitsInSphere(x,y,z, 1000)
                    local safe = true
                    for k = 1, #nearUnits do
                        local enId = nearUnits[k]
                        if Spring.ValidUnitID(enId) then 
                            local unitTeamId = Spring.GetUnitTeam(enId)
                            if not Spring.AreTeamsAllied(unitTeamId, teamID)  then 
                                safe = false
                                break
                            end
                        end
                    end
                    if safe then 
                        units[id] = false
                    end
                end
            end
        end
    end
	return SUCCESS
end


function Reset(self)
	ClearState(self)
end
