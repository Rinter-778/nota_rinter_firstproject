function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Attack building",
		parameterDefs = {

			{ 
				name = "lugerID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "luger.id",
			},
		}
	}
end



-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.repeatOrder = nil
end

function Run(self, units, parameter)
	local id = parameter.lugerID
	local teamID = Spring.GetMyTeamID()

	if not Spring.ValidUnitID(id) then 
		return FAILURE
	end

	if self.repeatOrder == nil then 
		SpringGiveOrderToUnit(id, CMD.FIRE_STATE, {0}, {})
		self.repeatOrder = 1		
	end  

	if self.repeatOrder == 0 then 
		self.repeatOrder = 5

		local x,y,z = Spring.GetUnitPosition(id)
		local lugerPos = Vec3(x,y,z)
        local nearUnits = Spring.GetUnitsInSphere(x,y,z, 1350)
		local closestBuilding = nil 
		local dist = 99999999
		for k = 1, #nearUnits do
			local enId = nearUnits[k]
			if Spring.ValidUnitID(enId) then 
				local unitTeamId = Spring.GetUnitTeam(enId)
				if not Spring.AreTeamsAllied(unitTeamId, teamID)  then 
					local defId = Spring.GetUnitDefID(enId)
					if defId ~= nil and UnitDefs[defId].isBuilding then 

						local x,y,z = SpringGetUnitPosition(enId)
						local bPos = Vec3(x,y,z)
						if lugerPos:Distance(bPos) < dist then 
							dist = lugerPos:Distance(bPos)
							closestBuilding = enId
						end

					end					
				end
			end
		end

		if closestBuilding ~= nil then 
			SpringGiveOrderToUnit(id, CMD.ATTACK, {closestBuilding}, {})
		else 
			SpringGiveOrderToUnit(id, CMD.FIRE_STATE, {3}, {})
			return SUCCESS
		end

	end

	self.repeatOrder = self.repeatOrder - 1 

	return RUNNING
end


function Reset(self)
	ClearState(self)
end
