local sensorInfo = {
    name = "GetEnemies",
    desc = "Returns updated positions and definitions of enemies",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns updated positions and definitions of enemies
return function(lines)
    local corridors = Sensors.core.MissionInfo().corridors
    local myTeamId = Spring.GetMyTeamID()

    if lines == nil then 
        lines = {}
    end

    local topBattleLine = corridors.Top.points[1].position

    for i = 2, #corridors.Top.points do
        local pos = corridors.Top.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 500)
        local shouldBreak = false
        for k = 1, #units do
            local unitTeamId = Spring.GetUnitTeam(units[k])
            if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 

                topBattleLine = pos 
                shouldBreak = true
                break
            end
        end

        if shouldBreak then 
            break
        end
    end

    local midBattleLine = corridors.Middle.points[1].position

    for i = 2, #corridors.Middle.points do
        local pos = corridors.Middle.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 500)
        local shouldBreak = false
        for k = 1, #units do
            local unitTeamId = Spring.GetUnitTeam(units[k])
            if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 

                midBattleLine = pos 
                shouldBreak = true
                break
            end
        end

        if shouldBreak then 
            break
        end
    end

    local botBattleLine = corridors.Bottom.points[1].position

    for i = 2, #corridors.Bottom.points do
        local pos = corridors.Bottom.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 500)
        local shouldBreak = false
        for k = 1, #units do
           local unitTeamId = Spring.GetUnitTeam(units[k])
           if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 

                botBattleLine = pos
                shouldBreak = true
                break
            end
        end

        if shouldBreak then 
            
            break
        end
    end

    lines.Top = topBattleLine
    lines.Mid = midBattleLine
    lines.Bot = botBattleLine
    return lines
end