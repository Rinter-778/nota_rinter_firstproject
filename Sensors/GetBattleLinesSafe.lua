local sensorInfo = {
    name = "GetEnemies",
    desc = "Returns updated positions and definitions of enemies",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns updated positions and definitions of enemies
return function(lines)
    local corridors = Sensors.core.MissionInfo().corridors
    local myTeamId = Spring.GetMyTeamID()

    if lines == nil then 
        lines = {}
        lines.Mid = {pos = corridors.Middle.points[1].position, saveDir = Vec3(0,0,0)}
        lines.Mid.lugerPos = lines.Mid.pos
        lines.Bot = {pos = corridors.Bottom.points[1].position, saveDir = Vec3(0,0,0)}
        lines.Bot.lugerPos = lines.Bot.pos
        lines.Top = {pos = corridors.Top.points[1].position, saveDir = Vec3(0,0,0)}
        lines.Top.lugerPos = lines.Top.pos
    end
    
    for i = 7, #corridors.Top.points do
        local pos = corridors.Top.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 400)
        local shouldBreak = false

        for k = 1, #units do
            local unitTeamId = Spring.GetUnitTeam(units[k])
            if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 
                
                local prevPos = corridors.Top.points[i - 1].position
                local saveDir = (prevPos - pos):GetNormalized()
                lines.Top.saveDir = saveDir
                if corridors.Top.points[i].isStrongPoint then 
                    lines.Top.pos = pos + saveDir * 800
                else
                    lines.Top.pos  = pos + saveDir * 700
                end
                shouldBreak = true
                break
            else
                lines.Top.pos  = pos
            end

        end

        if shouldBreak then 
            break
        end
    end

    for i = 7, #corridors.Middle.points do
        local pos = corridors.Middle.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 400)
        local shouldBreak = false
        
        
        for k = 1, #units do
            local unitTeamId = Spring.GetUnitTeam(units[k])
            local defId = Spring.GetUnitDefID(units[k])
            if defId ~= nil and UnitDefs[defId].name == 'armmart' then 
                local x,y,z = Spring.GetUnitPosition(units[k])
                lines.Mid.lugerPos = Vec3(x,y,z)
            end
            if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 
                local prevPos = corridors.Middle.points[i - 1].position
                local saveDir = (prevPos - pos):GetNormalized()

                lines.Mid.saveDir = saveDir

                    lines.Mid.pos  = pos + saveDir * 500
                shouldBreak = true
                break
            else
                local prevPos = corridors.Middle.points[i - 1].position
                local saveDir = (prevPos - pos):GetNormalized()

                lines.Mid.saveDir = saveDir

                lines.Mid.pos  = pos + saveDir * 400
                
            end
        end

        if shouldBreak then 
            break
        end
    end

    for i = 7, #corridors.Bottom.points do
        local pos = corridors.Bottom.points[i].position
        local units = Spring.GetUnitsInSphere(pos.x, pos.y, pos.z, 400)
        local shouldBreak = false

        for k = 1, #units do
           local unitTeamId = Spring.GetUnitTeam(units[k])
           if not Spring.AreTeamsAllied(unitTeamId, myTeamId)  then 

                local prevPos = corridors.Bottom.points[i - 1].position
                local saveDir = (prevPos - pos):GetNormalized()
                lines.Bot.saveDir = saveDir
                if corridors.Bottom.points[i].isStrongPoint then 
                    lines.Bot.pos = pos + saveDir * 1000
                else
                    lines.Bot.pos  = pos + saveDir * 100
                end
                shouldBreak = true
                break
            else
                lines.Bot.pos = pos
            end
        end

        if shouldBreak then 
            
            break
        end
    end

    return lines
end