local sensorInfo = {
    name = "GetNPositionsAlongBorder2",
    desc = "Returns count points along specified border",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


local Echo = Spring.Echo
local mapX =  Game.mapSizeX --/ Game.squareSize -- Game.mapSizeX Divide by Game.squareSize to get engine's "mapDims" coordinates
local mapZ =  Game.mapSizeZ --/ Game.squareSize -- Game.mapY
local OFFSET = 20
-- @Returns count points along specified border
return function(border, count)
    local res = {}
    local b = border
    border = nil
    if b == "maxX" then
        local delta = mapZ / count
        local pos = delta/2
        while pos < mapZ do
            res[#res + 1] = Vec3(mapX - OFFSET,0,pos)
            pos = pos + delta
        end
        return res
    elseif b == "minX" then
        local delta = mapZ / count
        local pos = delta/2
        while pos < mapZ do
            res[#res + 1] = Vec3(OFFSET,0,pos)
            pos = pos + delta
        end
        return res
    elseif b == "maxZ" then
        local delta = mapX / count
        local pos = delta/2
        while pos < mapX do
            res[#res + 1] = Vec3(pos,0,mapZ - OFFSET)
            pos = pos + delta
        end
        return res
    elseif b == "minZ" then
        local delta = mapX / count
        local pos = delta/2
        while pos < mapX do
            res[#res + 1] = Vec3(pos,0,OFFSET)
            pos = pos + delta
        end
        return res
    end    
end