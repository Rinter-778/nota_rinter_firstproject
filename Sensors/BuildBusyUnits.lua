local sensorInfo = {
    name = "BuildBusyUnits",
    desc = "Collects all units without orders",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Collects all units without orders
return function()

    local teamID = Spring.GetMyTeamID()
    
    local myunits = Spring.GetTeamUnits(teamID)
    local busyUnits = {}
    busyUnits.shopped = false
    busyUnits.units = {}
    for i = 1, #myunits do
        local unitId = myunits[i]
        if  Spring.ValidUnitID(unitId) then
            busyUnits.units[unitId] = false
        end
    end

    return busyUnits
end