local sensorInfo = {
	name = "FurthestDistance",
	desc = "Return biggest distance a unit is from the commander",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Return biggest distance a unit is from the commander
return function()
	local commanderPos;
	local furthestDist = 0
	for i=1, #units do		
		local unitID = units[i]
		if UnitDefs[Spring.GetUnitDefID(unitID)].name == "armbcom" then
			local x,y,z = Spring.GetUnitPosition(unitID)
			commanderPos = Vec3(x,y,z)
		end
	end
	
	for i=1, #units do		
		local unitID = units[i]
		local x,y,z = Spring.GetUnitPosition(unitID)
		local newDist = Vec3(x,y,z):Distance(commanderPos)
		if newDist > furthestDist then
			furthestDist = newDist
		end
	end
	
	return furthestDist
end