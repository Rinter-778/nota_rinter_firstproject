function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load all units into transporter",
		parameterDefs = {
			{ 
				name = "customUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "loaderID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.loadedUnits = {}
end

function Run(self, units, parameter)
	local loaderID = parameter.loaderID -- id
	local loaderDef = UnitDefs[Spring.GetUnitDefID(loaderID)]
	local lx,ly,lz = SpringGetUnitPosition(loaderID)
	local loaderPos = Vec3(lx,ly,lz)
	local loadRadius = loaderDef.loadingRadius
	
	local locUnits = {}
	if parameter.customUnits == nil then
		locUnits = units
	else
		locUnits = parameter.customUnits
	end
	
	if self.loadedUnits == nil then
		self.loadedUnits = {}
	end
	
	-- validation
	if loaderDef.transportCapacity == 0 then
		return FAILURE
	end
	
	local isSuccess = true
	local loaderHasMoveCommand = false;
	for i = 1, #locUnits do
		local unitID = locUnits[i]
		local unitDef = UnitDefs[Spring.GetUnitDefID(unitID)]
		local x, y, z = SpringGetUnitPosition(unitID)
		local unitPos = Vec3(x,y,z)	
		local unitTransID = Spring.GetUnitTransporter(unitID)
		
		if (unitID ~= loaderID and unitTransID ~= loaderID and not unitDef.cantBeTransported ) then
			isSuccess = false
			local distance = unitPos:Distance(loaderPos)
			if(distance > loadRadius) then
				if not loaderHasMoveCommand then
					SpringGiveOrderToUnit(loaderID, CMD.MOVE, unitPos:AsSpringVector(),{})
					loaderHasMoveCommand = true
				end
				--SpringGiveOrderToUnit(unitID, CMD.MOVE, loaderPos:AsSpringVector(),{})
			else--if self.loadedUnits[unitID] == nil then
				self.loadedUnits[unitID] = true
				SpringGiveOrderToUnit(loaderID, CMD.LOAD_UNITS, {unitID}, {})
			end
		end
	end
	
	if isSuccess then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
