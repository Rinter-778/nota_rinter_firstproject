local sensorInfo = {
    name = "GetAvailableFark",
    desc = "Re",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns true if there are less then 6 active farks
return function(farkList)
    local ret
    for id, v in pairs(farkList) do
        if Spring.ValidUnitID(id) and not v.taken then
            v.taken = true
            ret = v
        end
    end
    return ret
end