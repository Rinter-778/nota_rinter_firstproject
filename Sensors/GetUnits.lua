local sensorInfo = {
	name = "GetUnits",
	desc = "Returns units array",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Returns units array
return function()
	local result = {}
	for i = 1, #units do
		result[i] = units[i]
	end
	return result
end