local sensorInfo = {
	name = "DistanceToNearestEnemy",
	desc = "DistanceToNearestEnemy",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition

-- @DistanceToNearestEnemy
return function(unitID)
	local x,y,z = SpringGetUnitPosition(unitID)
	local pos = Vec3(x,y,z)
	local enemyID = Spring.GetUnitNearestEnemy(unitID)
	if enemyID == nil then
		return 99999
	end
	local ex,ey,ez = SpringGetUnitPosition(enemyID)	
	return Vec3(ex,ey,ez):Distance(pos)
end