local sensorInfo = {
    name = "GetEnemies",
    desc = "Returns updated positions and definitions of enemies",
    author = "Patrik",
    date = "2021-05-11",
    license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load



local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


-- @Returns updated positions and definitions of enemies
return function(enemiesPositions)
    local enemies = Sensors.core.EnemyUnits()

    if enemiesPositions == nil then
        enemiesPositions = {}
        enemiesPositions.enemies = {}
        enemiesPositions.dirty = false
    end

    for i = 1, #enemies do
        local enId = enemies[i]
        local defID = Spring.GetUnitDefID(enId)
        local x,y,z = Spring.GetUnitPosition(enId)
        if defID ~= nil then 
            if enemiesPositions.enemies[defID] == nil then
                enemiesPositions.enemies[defID] = {}
            end

            if enemiesPositions.enemies[defID][enId] == nil then
                enemiesPositions.dirty = true
                enemiesPositions.enemies[defID][enId] = {}
                enemiesPositions.enemies[defID][enId].dirty = true
            end

            enemiesPositions.enemies[defID][enId].pos = Vec3(x,y,z)
        end

    end

    return enemiesPositions
end