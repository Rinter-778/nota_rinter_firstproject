local sensorInfo = {
	name = "IsCommander",
	desc = "Returns true if the on of the units is a commander",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Returns true if the on of the units is a commander
return function()
	for i,unit in ipairs(units) do
		if UnitDefs[Spring.GetUnitDefID(unit)].name == "armbcom" then
			return true
		end
	end
	return false
end