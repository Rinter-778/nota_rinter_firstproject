local sensorInfo = {
	name = "GetFormation",
	desc = "Returns formation of units",
	author = "Patrik",
	date = "2021-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Returns formation of units
return function()
	local result = {
	  yaw,
	  unitList = {}, -- array of unitIDs
	  formationDefinition = {}, -- array of Vec3()
	}
	local commanderPos
	local commanderYaw = 0
	
	for i=1, #units do		
		local unitID = units[i]
		if UnitDefs[Spring.GetUnitDefID(unitID)].name == "armbcom" then
			local x,y,z = Spring.GetUnitPosition(unitID)
			commanderPos = Vec3(x,y,z)
			local pitch, yaw, roll = Spring.GetUnitRotation(unitID)
			commanderYaw = yaw
			result.yaw = yaw
			result.unitList[unitID] = 1
			break
		end
	end
	
	
	local cnt = 2;
	for i=1, #units do		
		local unitID = units[i]
		if UnitDefs[Spring.GetUnitDefID(unitID)].name ~= "armbcom" then
			cnt = cnt + 1
			result.unitList[unitID] = cnt
		end
	end
	
	
	for unitID, unitIndex in pairs(result.unitList) do
		if unitIndex == 1 then 
			result.formationDefinition[unitIndex] = Vec3(0,0,0)
		else
			result.formationDefinition[unitIndex] = Vec3(40,0,(unitIndex - #units/2 - 1) * 40):Rotate2D(commanderYaw * 180/3.14)
		end
	end
	
	return result
end