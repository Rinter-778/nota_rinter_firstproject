-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Buy attack squad",
        parameterDefs = {
            { 
                name = "boughtUnits",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "name",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

function Run(self, units, parameter)
    local pos = parameter.retSquad;


    message.SendRules({
        subject = "swampdota_buyUnit",
        data = {
            unitName = parameter.unitName
        },
    })
    Spring.GiveOrderToUnit(parameter.unit, CMD.BUILD,{pos.x,pos.y,pos.z, } ,{})

    return SUCCESS
end


function Reset(self)
    return self
end